import React from "react";
import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';


export default function Banner() {
	return (
	/*	<div className="container-fluid">
			<div className="row justify-content-center">
				<div className="col">
					<div className="jumbotron">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere.</p>
						<Button className="btn btn-primary">Enroll</Button>
					</div>
				</div>
			</div>
		</div>
	)*/
	<Container fluid>
		<Row>
			<Col className="px-0">
				<Jumbotron fluid className="px-3">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere.</p>
					<Button className="btn btn-primary">Enroll now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	</Container>
)
}