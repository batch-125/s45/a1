import React, {useState} from 'react';
import PropTypes from 'prop-types';//nakay react na talaaga to. validate if nakakareceive ng values

// react-bootstrap components
import {Card,Button} from 'react-bootstrap';

export default function Course(props){
	
	console.log(props)
	let course = props.course

	// state
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);

	function enroll(){
		if(count < 10){
			setCount( count + 1)
			setSeat( seat - 1)
		} else {
			alert("No more seats")
		}
		
	}


	return (
		<Card>
			<Card.Body>
				<Card.Title>{course.name}</Card.Title>
					<h5>Description</h5>
					<p>{course.description}</p>
					<h5>Price:</h5>
					<p>{course.price}</p>
					<h5>Enrollees</h5>
					<p>{count} Enrollees</p>
					<p>{seat} Seats</p>
					<Button variant="primary" onClick={
						enroll
						// () => {
						// 	count < 30 
						// 	? setCount( count + 1) 
						// 	: alert("No Slot Available")
						// }
					}>Enroll
					</Button>
			</Card.Body>
		</Card>
	)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}