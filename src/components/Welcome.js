import React from 'react';

export default function Welcome(props){
	console.log(props) // {name: "John"}
	return(
	<div>
		<h1>Hello, {props.name}</h1>
	</div>
	)
}