import React from "react";

// react bootstrap
import {Navbar,Nav} from 'react-bootstrap';
// import Nav from 'react-bootstrap/Nav';

// app navbar
export default function AppNavbar(){
  return (
    <Navbar bg="info" expand="lg">
    <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#link">Course</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}