import React from 'react';


// react-bootstrap components
import {Container} from 'react-bootstrap';
// components
import Course from './../components/Course'; //itong Course is how it will be displayed

// mock data
import courses from './../mock-data/courses';

export default function Courses(){

	let CourseCards = courses.map((course) =>{

		return <Course key={course.id} course={course}/>
		// key={course.id} purpose is
	}) 

	return(
		<Container fluid>
			{CourseCards}
		</Container>
	)
}